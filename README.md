# Behavior Subject
Uma outra forma de trafegar dados entre componentes utilizando serviços.



## Problema

A maneira mais simples de trafegar dados entre componentes é utilizando inbound property (@Input) e EventEmitter (@Output). Muitas vezes isso é suficiente, porém, podemos passar por alguns problemas:

- E se quiséssemos alterar o dado da variável que estamos passando via inbound property? Teríamos que renderizar o componente novamente?
- E se quiséssemos executar alguma lógica quando detectarmos alteração no valor?
- E se o componente filho alterar esse dado? A única alternativa é criar um "caminho de volta" para avisar o componente pai?

Será que existe uma forma de manter uma fonte centralizada desse dado caso vários componentes precisem dele?

Uma das alternativas que temos para solucionar esses problemas é fazendo uso do BehaviorSubject ao invés de passar propriedades comuns para outros componentes. Seu funcionamento lembra o EventEmitter, mas trata-se de um Observable que sempre será instanciado e inicializado com um valor. Poderemos passar este BehaviorSubject para outros componentes via inbound property ou centralizá-lo em um serviço responsável pela comunicação, dessa forma, vários componentes poderão se inscrever(subscribe) nessa fonte de dados e manterem-se atualizados do seu último valor.



## Solução

Em um componente pai ou serviço, devemos criar e instanciar um BehaviorSubject que será disponibilizado para outros componentes ou serviços. Novos valores são enviados ao BehaviorSubject através do método next(). Com o subscribe(), podemos detectar quando houve alterações. Podemos obter o valor atual durante o próprio subscribe() e passá-lo a alguma variável ou acessando a propriedade value do BehaviorSubject: nomeBehaviorSubject.value. Também podemos exibir o valor no template html utilizando a pipe async: nomeBehaviorSubject | async.



## Run Project
Clone este repositório para a máquina local
```
git clone https://caruzojr@bitbucket.org/caruzojr/behavior-subject.git
```
Dentro da pasta do projeto, execute o comando abaixo para baixar todas as dependencias necessárias
```
npm install
```
Rode o seguinte comando para executar o projeto
```
ng s --open
```
