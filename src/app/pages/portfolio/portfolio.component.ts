import { Component, OnInit } from '@angular/core';

import { HeaderService } from '../../shared/services/header.service';

@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.scss']
})
export class PortfolioComponent implements OnInit {

  constructor(
    private headerService: HeaderService
  ) { }

  ngOnInit(): void {

    this.headerService.showTitleHeader(true);
    this.headerService.titleHeader('Vejá os meus melhores projetos!');

  }

}
