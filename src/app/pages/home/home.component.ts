import { Component, OnInit } from '@angular/core';

import { HeaderService } from '../../shared/services/header.service';
import { FooterService } from '../../shared/services/footer.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private headerService: HeaderService,
    private footerService: FooterService
  ) { }

  ngOnInit(): void {
    this.headerService.showTitleHeader(true);
    this.headerService.titleHeader('Olá seja bem-vindo ao nosso teste!');

    this.footerService.showButtonFooter(true);
    this.footerService.titleButtonFooter('Avançar');
  }

}
