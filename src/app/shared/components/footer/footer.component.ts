import { Component, OnInit } from '@angular/core';
import { FooterService } from './../../services/footer.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  public hasShowButton: boolean;
  public titleButton: string;

  constructor(
    private footerService: FooterService
  ) { }

  ngOnInit() {

    this.footerService.showButtonObservable.subscribe((result) =>{
      this.hasShowButton = result;
    })

    this.footerService.titleButtonObservable.subscribe((result) => {
      this.titleButton = result;
    })
  }

}
