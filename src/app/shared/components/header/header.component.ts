import { Component, OnInit } from '@angular/core';
import { HeaderService } from './../../services/header.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public hasShowTitle: boolean;
  public title: string;

  constructor(
    private headerService: HeaderService
  ) { }

  ngOnInit(): void {

    this.headerService.showTitleObservable.subscribe((result) => {
      this.hasShowTitle = result;
    });

    this.headerService.titleObservable.subscribe((result) => {
      this.title = result;
    });

  }

}
