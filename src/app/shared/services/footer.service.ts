import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FooterService {

  public showButton = new BehaviorSubject<boolean>(false);
  public titleButton = new BehaviorSubject<string>('');

  showButtonObservable = this.showButton.asObservable();
  titleButtonObservable = this.titleButton.asObservable();


  showButtonFooter(value: boolean): void {
    this.showButton.next(value);
  }

  titleButtonFooter(value: string): void {
    this.titleButton.next(value);
  }
}
