import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HeaderService {

  public showTitle = new BehaviorSubject<boolean>(false);
  public title = new BehaviorSubject<string>('');

  showTitleObservable = this.showTitle.asObservable();
  titleObservable = this.title.asObservable();


  showTitleHeader(value: boolean): void {
    this.showTitle.next(value);
  }

  titleHeader(value: string): void {
    this.title.next(value);
  }

}
